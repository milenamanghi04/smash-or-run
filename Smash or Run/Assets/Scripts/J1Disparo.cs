using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class J1Disparo : MonoBehaviour
{

    public GameObject Bullet;

    public Transform spawnPoint;

    public float shotForce = 0f;
    public float shotRate = 0f;

    private float shotRateTime = 0f;
    public float SecondDestroy = 0f;


    void Update()
    {

        if (Input.GetKey(KeyCode.E))
        {

            if (Time.time > shotRateTime)
            {
                GameObject newBullet;

                newBullet = Instantiate(Bullet, spawnPoint.position, spawnPoint.rotation);

                newBullet.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * shotForce);

                shotRateTime = Time.time + shotRate;

                Destroy(newBullet, SecondDestroy);

            }



        }



    }
}
