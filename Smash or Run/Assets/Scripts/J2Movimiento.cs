using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class J2Movimiento : MonoBehaviour
{

    public float speed = 5f;
    public float rotationSpeed = 5f;
    Vector3 movementInput;
 


    void Update()
    {

        movementInput = Vector3.zero;

        if (Input.GetKey("up"))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey("down"))
        {
            movementInput.z = -1;
        }

        if (Input.GetKey("right"))
        {
            movementInput.x = 1;
        }
        else if (Input.GetKey("left"))
        {
            movementInput.x = -1;
        }




        Vector3 movementDirection = movementInput;
        movementDirection.Normalize();

        transform.position = transform.position + movementDirection * speed * Time.deltaTime;

        if (movementDirection != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementDirection), rotationSpeed * Time.deltaTime);
        }



    }

    private void OnCollisionEnter(Collision collision)
    {
       
    }



}

