using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class J1Movimiento : MonoBehaviour
{

    private float speed = 5f;
    public float baseSpeed = 0f;
    public float rotationSpeed = 5f;
    Vector3 movementInput;

    public bool vivo;

  
  


    public float dashingTime = 0.2f;
    public float dashForce = 20f;
    public float timeDash = 1f;

    private bool isDashing;
    private bool canDash = true;

 

    void Start()
    {
        speed = baseSpeed;

    }


    void Update()
    {
        Move();
        Dash();

     


        
       

   

    }


    public void Move()
    {

        movementInput = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movementInput.z = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            movementInput.z = -1;
        }

        if (Input.GetKey(KeyCode.D))
        {
            movementInput.x = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            movementInput.x = -1;
        }




        Vector3 movementDirection = movementInput;
        movementDirection.Normalize();

        transform.position = transform.position + movementDirection * speed * Time.deltaTime;

        if (movementDirection != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementDirection), rotationSpeed * Time.deltaTime);
        }
    }



    public void Dash()
    {




        if (Input.GetKey(KeyCode.Space))
        {
            if(canDash == true)
            {
               
                isDashing = true;
               canDash = false;

              speed = dashForce;

            }
            
          
        }
      
    
        if(isDashing == true)
        {

             dashingTime -= Time.deltaTime;
          if (dashingTime <= 0)
          {
                 isDashing = false;
                 speed = baseSpeed;
                 dashingTime = 0.2f;





          }

        }

        timeDash -= Time.deltaTime;
        if (timeDash <= 0)
        {
            canDash = true;
            timeDash = 1f;
        }



    }





}
